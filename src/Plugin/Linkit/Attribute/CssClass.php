<?php

/**
 * @file
 * Contains \Drupal\linkit\Plugin\Linkit\Attribute\CssClass.
 */

namespace Drupal\vb_core\Plugin\Linkit\Attribute;

use Drupal\linkit\AttributeBase;

/**
 * Class attribute.
 *
 * @TODO: For now Drupal filter_html wont support class attributes with
 * wildcards.
 * See: \Drupal\filter\Plugin\Filter\FilterHtml::getHTMLRestrictions
 * See: core/modules/filter/filter.filter_html.admin.js
 *
 * @Attribute(
 *   id = "class",
 *   label = @Translation("Class"),
 *   html_name = "class",
 *   description = @Translation("Select field for the class attribute."),
 * )
 */
class CssClass extends AttributeBase {

 /**
  * {@inheritdoc}
  */
 public function buildFormElement($default_value) {
   return [
     '#type' => 'select',
     '#title' => t('Class'),
     '#default_value' => $default_value,
     '#options' => [
     	'' => $this->t('None'),
     	'btn btn-primary' => $this->t('Button primary'),
     	'btn btn-default' => $this->t('Button default'),
     	'btn btn-success' => $this->t('Button success'),
      'btn btn-link' => $this->t('Button link'),
     ]
   ];
 }

}
