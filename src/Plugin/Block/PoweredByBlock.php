<?php

namespace Drupal\vb_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a 'Powerd by vector bross' block.
 *
 * @Block(
 *   id = "powered_by_block",
 *   admin_label = @Translation("Powered by Block"),
 *   category = @Translation("Vector BROSS"),
 * )
 */
class PoweredByBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'powered_by_message' => 'powered_by',
      'override_destination_url' => NULL,
      'title_attribute' => 'Website laten maken',
    ];
  }

  /**
   * Returns the 'Powered by' options.
   *
   * @return array
   */
  protected function getPoweredByOptions() {
    return [
      'powered_by' => $this->t('Powered by vector bross'),
      'design_by' => $this->t('Design by vector bross'),
      'webdesign_by' => $this->t('Webdesign by vector bross'),
      'proudly_powered_by' => $this->t('Proudly powered by vector bross'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['powered_by_message'] = [
      '#type' => 'select',
      '#title' => $this->t('Style'),
      '#options' => $this->getPoweredByOptions(),
      '#default_value' => $config['powered_by_message'] ?? 'powered_by',
    ];

    $form['override_destination_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Override destination url'),
      '#default_value' => $config['override_destination_url'] ?? NULL,
    ];

    $form['title_attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link title attribute'),
      '#default_value' => $config['title_attribute'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['powered_by_message'] = $values['powered_by_message'];
    $this->configuration['override_destination_url'] = $values['override_destination_url'];
    $this->configuration['title_attribute'] = $values['title_attribute'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $link_text_key = $this->getConfiguration()['powered_by_message'];
    $link_text = $this->getPoweredByOptions()[$link_text_key];

    $options = [
      'query' => [
        'utm_source' => \Drupal::request()->getHost(),
        'utm_medium' => 'website',
        'utm_content' => 'powered_by',
      ],
    ];

    $url = Url::fromUri('https://www.v-b.be/nl', $options);

    if (isset($this->getConfiguration()['override_destination_url']) && $this->getConfiguration()['override_destination_url'] != NULL) {
      $url = Url::fromUri($this->getConfiguration()['override_destination_url'], $options);
    }

    $build = [
      '#type' => 'link',
      '#title' => $link_text,
      '#url' => $url,
      '#options' => [
        'attributes' => [
          'target' => '_blank',
          'class' => ['vb-design'],
        ],
      ],

    ];

    // Add title attribute to link.
    if (isset($this->getConfiguration()['title_attribute']) && $this->getConfiguration()['title_attribute'] != NULL) {
      $build['#options']['attributes']['title'] = $this->getConfiguration()['title_attribute'];
    }

    // Only dofollow links on homepage.
    if (!\Drupal::service('path.matcher')->isFrontPage()) {
      $build['#options']['attributes']['rel'] = 'nofollow';
    }

    return $build;

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
