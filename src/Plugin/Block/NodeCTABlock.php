<?php

namespace Drupal\vb_core\Plugin\Block;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\node\NodeInterface;

/**
 * Provides a 'Node CTA' block.
 *
 * @Block(
 *   id = "node_cta_block",
 *   admin_label = @Translation("Node CTA Block"),
 *   category = @Translation("Vector BROSS"),
 * )
 */
class NodeCTABlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Check if block is overridden by field_cta on current node.
    $node = \Drupal::routeMatch()->getParameter('node');

    if ($node instanceof NodeInterface) {
      // Check if CTA should be disabled or not.
      if ($node->hasField('field_disable_cta') && !$node->get('field_disable_cta')->isEmpty()) {
        $disable_cta_value = $node->get('field_disable_cta')->getString();
        if ($disable_cta_value == '1') {
          return [];
        }
      }

      // Get overridden block.
      if ($node->hasField('field_cta') && !$node->get('field_cta')->isEmpty()) {
        $block_id = $node->get('field_cta')->getValue()[0]['target_id'];
      }

      // If not overridden, check if there is a default value set in vb_core.node_config.settings.
      if (!isset($block_id)) {
        $config = \Drupal::configFactory()->get('vb_core.node_config.settings');
        $block_id = $config->get('node_cta_default_' . $node->bundle());
      }
    }

    if (isset($block_id)) {
      // If there is a block ID, load block_content entity.
      $block_content = \Drupal::entityTypeManager()->getStorage('block_content')
        ->load($block_id);
      if($block_content instanceof BlockContentInterface) {
        $build = \Drupal::entityTypeManager()->getViewBuilder('block_content')->view($block_content);
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }
    else {
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

}
