<?php

namespace Drupal\vb_core\EventSubscriber;

use Drupal\default_content\Event\ImportEvent;
use Drupal\field\Entity\FieldConfig;
use Drupal\vb_core\Event\NodeTypeInsert;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class EventSubscriber.
 *
 * @package Drupal\vb_core
 */
class VBCoreSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['setHeaderCacheControl', -10];
    $events[NodeTypeInsert::NODE_TYPE_INSERT] = ['onNodeTypeInsert'];
    $events['default_content.import'] = ['onContentImport'];

    return $events;
  }

  /**
   * Set http cache control headers.
   */
  public function setHeaderCacheControl(ResponseEvent $event) {
    $response = $event->getResponse();

    if (!$response->isCacheable()) {
      return;
    }

    $ttl = $response->getMaxAge();

    switch ($response->getStatusCode()) {
      case 404:
        // 404-pages should be cached for max. 1h.
        $ttl = '3600';
        break;
    }

    if ($ttl != $response->getMaxAge()) {
      $response->setClientTtl($ttl);
      $response->setSharedMaxAge($ttl);
    }
  }

  /**
   * A method to be called after node type insert.
   *
   * It sets the default fields for that content-type.
   *
   * @param \Drupal\vb_core\Event\NodeTypeInsert $event
   *   The event triggered by the request.
   */
  public function onNodeTypeInsert(NodeTypeInsert $event) {
    $entity = $event->getEntity();

    /* @var $entity_field_manager \Drupal\Core\Entity\EntityFieldManager */
    $entity_field_manager = \Drupal::service('entity_field.manager');

    // Define default fields we would like to copy from CT Page.
    $default_fields = [
      'field_summary',
      'field_seo_description',
      'field_seo_title',
      'field_media_image',
      'field_metatags',
      'field_paragraphs',
    ];

    // Get field definitions from node type page.
    $page_fields = $entity_field_manager->getFieldDefinitions('node', 'page');

    foreach ($default_fields as $default_field) {
      /** @var \Drupal\field\Entity\FieldConfig $default_field */

      if (isset($page_fields[$default_field])) {
        // Fields storage exists and field is available on CT page.
        // We can create the field at new CT we are creating now.
        $field_config = FieldConfig::Create([
          'entity_type' => 'node',
          'field_name' => $default_field,
          'bundle' => $entity->id(),
          'label' => $page_fields[$default_field]->label(),
          'required' => $page_fields[$default_field]->isRequired(),
          'description' => $page_fields[$default_field]->getDescription(),
          'translatable' => $page_fields[$default_field]->isTranslatable(),
          'settings' => $page_fields[$default_field]->getSettings()
        ])->save();
      }
    }
  }

  /**
   * A method to be called whenever a default_content.import event is dispatched.
   *
   * It sets the overview_page for the imported default content.
   *
   * @param \Drupal\default_content\Event\ImportEvent $event
   *   The event triggered by the request.
   */
  public function onContentImport(ImportEvent $event) {
    $module = $event->getModule();

    // Specific config after importing from vb_core_default_content.
    if ($module == 'vb_core_default_content') {
      $disable = TRUE;
      $config_factory = \Drupal::configFactory();

      // Set homepage.
      $home_node = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['title' => 'Home', 'type' => 'page']);

      if (!empty($home_node)) {
        $home_node = reset($home_node);

        $config_factory
          ->getEditable('system.site')
          ->set('page.front', '/node/' . $home_node->id())
          ->save();
      }
    }

    // Submodules, add overview_page config.
    if (function_exists($module . '_extra_config')) {
      $disable = TRUE;
      $extra_config_function = $module . '_extra_config';
      $extra_config = $extra_config_function();

      $config_factory = \Drupal::configFactory();
      $config = $config_factory->getEditable('vb_core.node_config.settings');
      $node_storage = \Drupal::entityTypeManager()->getStorage('node');

      $query = \Drupal::entityQuery('node')
        ->condition('type', $extra_config['overview_node_type'])
        ->condition('title', $extra_config['overview_node_title'])
        ->accessCheck(TRUE)
        ->execute();

      if (!empty($query)) {
        $config->set('overview_page_' . $extra_config['node_type'], reset($query));
        $config->save(TRUE);

        $query = \Drupal::entityQuery('node')
          ->condition('type', $extra_config['node_type'])
          ->accessCheck(TRUE)
          ->execute();
        $child_nodes = $node_storage->loadMultiple($query);

        foreach ($child_nodes as $child_node) {
          \Drupal::service('pathauto.generator')->updateEntityAlias($child_node, 'update');
        }
      }
    }

    // Disable module after import.
    if (isset($disable)) {
      \Drupal::service('module_installer')->uninstall([$module]);
    }
  }

}
