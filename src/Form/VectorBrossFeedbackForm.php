<?php

namespace Drupal\vb_core\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\vb_core\Constants\VbCoreConstants;

/**
 * Provides a VB Core settings form for the node types.
 */
class VectorBrossFeedbackForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vector_bross_feedback_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['feedback_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'feedback-container',
      ],
    ];
    $form['feedback_container']['feedback_replace'] = [
      '#prefix' => "<div id='vb-feedback-replace'>",
      '#suffix' => "</div>",
      '#type' => 'container',
    ];
    $form['feedback_container']['feedback_replace']['intro'] = [
      '#type' => 'markup',
      '#markup' => $this->t('To continuously improve our service, we would appreciate your feedback. How many stars do you give your cooperation with Vector Bross?'),
    ];

    $form['feedback_container']['feedback_replace']['stars'] = [
      '#type' => 'radios',
      '#title' => $this->t('Stars'),
      '#options' => [
        '1' => $this->t('1 star'),
        '2' => $this->t('2 stars'),
        '3' => $this->t('3 stars'),
        '4' => $this->t('4 stars'),
        '5' => $this->t('5 stars'),
      ],
      '#ajax' => [
        'callback' => '::processFeedbackCallback',
        'disable-refocus' => FALSE, // Or TRUE to prevent re-focusing on the triggering element.
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['#attached']['library'][] = 'vb_core/vectorbross_feedback';

    return $form;
  }

  /**
   * The form ajax callback.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function processFeedbackCallback(array $form, FormStateInterface $form_state) {
    $uid = \Drupal::currentUser()->id();
    $values = [
      'timestamp' => time(),
      'stars' => $form_state->getValue('stars'),
    ];

    \Drupal::keyValue('vb_core_vectorbross_feedback')->set($uid, $values);

    if ($form_state->getValue('stars') >= 4) {
      $url = Url::fromUri(VbCoreConstants::VB_ADD_REVIEW_URL);
      $review_link = Link::fromTextAndUrl(t('Write Google review'), $url)->toString();
      $rating_class = 'rating-good';
      $warning = t('We are pleased to learn that you are satisfied from our cooperation. We would appreciate it if you could leave a Google review. %review_link', ['%review_link' => $review_link]);
    }
    else {
      $host = \Drupal::request()->getHost();
      $url = Url::fromUri('mailto:' . VbCoreConstants::VB_FEEDBACK_HELP_MAIL . '?subject=Vector bross Feedback from ' . $host);
      $mail_link = Link::fromTextAndUrl(VbCoreConstants::VB_FEEDBACK_HELP_MAIL, $url)->toString();
      $rating_class = 'rating-bad';
      $warning = t('We regret that you are not completely satisfied with your cooperation with Vector Bross. If you wish, you can further explain your given score at %mail.', ['%mail' => $mail_link]);
    }

    $feedback_replace = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $warning,
      '#attributes' => [
        'class' => [$rating_class],
      ],
    ];

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#vb-feedback-replace', $feedback_replace));

    return $response;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
