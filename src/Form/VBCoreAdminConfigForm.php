<?php

namespace Drupal\vb_core\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a VB Core Admin config form.
 */
class VBCoreAdminConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_core_admin_config_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_core.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_core.settings');

    $form['dev'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Development active'),
      '#default_value' => $config->get('dev'),
    ];

    $form['db-max'] = [
      '#type' => 'number',
      '#field_suffix' => 'MB',
      '#title' => $this->t('Maximum database size'),
      '#min' => 100,
      '#max' => 10000,
      '#step' => 50,
      '#default_value' => $config->get('db-max'),
    ];

    $form['no_email_username'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Don\'t use email-address as username.'),
      '#default_value' => $config->get('no_email_username'),
    ];

    $form['langswitcher_disable_untranslated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable links to untranslated pages in language switcher.'),
      '#default_value' => $config->get('langswitcher_disable_untranslated'),
    ];

    $form['disable_vb_feedback'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable vector bross feedback on admin-pages.'),
      '#default_value' => $config->get('disable_vb_feedback'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configFactory->getEditable('vb_core.settings')
      ->set('dev', $values['dev'])
      ->set('db-max', $values['db-max'])
      ->set('no_email_username', $values['no_email_username'])
      ->set('langswitcher_disable_untranslated', $values['langswitcher_disable_untranslated'])
      ->set('disable_vb_feedback', $values['disable_vb_feedback'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
