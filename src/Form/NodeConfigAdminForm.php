<?php

namespace Drupal\vb_core\Form;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a VB Core settings form for the node types.
 */
class NodeConfigAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_config_admin_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_core.node_config.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_core.node_config.settings');

    $node_bundles = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    // Foreach node bundles, we create tab.
    $form['node_types'] = [
      '#type' => 'vertical_tabs',
    ];

    // Get all reusable content blocks.
    $content_blocks = \Drupal::entityTypeManager()->getStorage('block_content')
      ->loadByProperties(['reusable' => TRUE, 'status' => TRUE]);

    $content_block_options = [];
    foreach ($content_blocks as $content_block) {
      /** @var BlockContentInterface $content_block */
      $content_block_options[$content_block->id()] = $content_block->label();
    }

    // Loop through all node bundles.
    foreach ($node_bundles as $node_bundle) {
      $bundle = $node_bundle->id();
      $label = $node_bundle->label();
      $tab = 'node_type_' . $bundle;

      // Create a grouping element using a fieldset.
      $form['node_types'][$tab] = [
        '#type' => 'details',
        '#title' => $label,
        '#group' => 'node_types',
      ];

      // Overview pages.
      $default_entity = FALSE;
      if ($config->get('overview_page_' . $bundle)) {
        $default_entity = \Drupal::entityTypeManager()->getStorage('node')
          ->load($config->get('overview_page_' . $bundle));
      }

      $form['node_types'][$tab]['overview_page_' . $bundle] = [
        '#type' => 'entity_autocomplete',
        '#title' => $this->t('Overview page ') . $label,
        '#target_type' => 'node',
        '#tags' => TRUE,
        '#size' => 30,
        '#maxlength' => 1024,
        '#default_value' => $default_entity ?? NULL,
      ];

      // Social sharing
      $form['node_types'][$tab]['social_sharing'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Social sharing'),
        '#description' => $this->t('Select the social media you want the node type to be sharable on.'),
      ];

      $form['node_types'][$tab]['social_sharing']['social_media_' . $bundle] = [
        '#type' => 'checkboxes',
        '#options' => [
          'facebook' => 'Facebook',
          'linkedin' => 'Linked In',
          'twitter' => 'Twitter',
          'pinterest' => 'Pinterest',
          'whatsapp' => 'Whatsapp',
        ],
        '#default_value' => $config->get('social_media_' . $bundle) ?? [],
      ];

      // Field display
      $form['node_types'][$tab]['fields'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Fields to show'),
        '#description' => $this->t('Select which fields you would like ot show oin the detail page.'),
      ];

      $form['node_types'][$tab]['fields']['show_author_' . $bundle] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Show author'),
        '#default_value' => $config->get('show_author_' . $bundle) ?? '',
      ];
      $form['node_types'][$tab]['fields']['show_date_' . $bundle] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Show date'),
        '#default_value' => $config->get('show_date_' . $bundle) ?? '',
      ];
      $form['node_types'][$tab]['fields']['show_image_' . $bundle] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Show image'),
        '#default_value' => $config->get('show_image_' . $bundle) ?? '',
      ];

      // Set allowed CTA-blocks for a content type. This will fill the select list on node edit page?
      $form['node_types'][$tab]['cta'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Call to action'),
      ];
      $form['node_types'][$tab]['cta']['node_cta_' . $bundle] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Available CTA-blocks for node type') . ' '. $label,
        '#description' => $this->t('Set the available CTA-blocks a content editor can use to override per node.'),
        '#options' => $content_block_options,
        '#default_value' => $config->get('node_cta_' . $bundle) ?? [],
      ];

      // Set default CTA-block visible on each node of a specific content-type.
      $form['node_types'][$tab]['cta']['node_cta_default_' . $bundle] = [
        '#type' => 'select',
        '#title' => $this->t('Default CTA-block for node type') . ' '. $label,
        '#description' => $this->t('Set the default CTA-block shown on this node type. If an override-field is available on the node type, this value can be overridden.'),
        '#options' => ['' => $this->t('None')] + $content_block_options,
        '#default_value' => $config->get('node_cta_default_' . $bundle) ?? [],
      ];

    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('vb_core.node_config.settings');

    $data = [];
    foreach ($values as $key => $value) {
      $data[$key] = $value;

      // Override, we only need node id.
      if (str_starts_with($key, 'overview_page_') && isset($value[0]['target_id'])) {
        $data[$key] = $value[0]['target_id'];
      }
    }

    $config->setData($data);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
