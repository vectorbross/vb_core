<?php

namespace Drupal\vb_core\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a VB Core settings form for the node types.
 */
class SiteInfoConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_info_config_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_core.site_info_config.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_core.site_info_config.settings');

    // Create tabs.

    // During profile-install, we use horizontal tabs.
    $build_info = $form_state->getBuildInfo();
    if (isset($build_info['args'][0]['active_task']) && $build_info['args'][0]['active_task'] == 'vb_profile_set_client_information') {
      $tabs_type = 'horizontal_tabs';
    }

    $form['tabs'] = [
      '#type' => $tabs_type ?? 'vertical_tabs',
    ];

    // Client information tab.
    $form['tabs']['client_info'] = [
      '#type' => 'details',
      '#title' => $this->t('Client information'),
      '#group' => 'tabs',
    ];

    // Contact-info
    $form['tabs']['client_info']['contact'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Contact information'),
    ];

    $form['tabs']['client_info']['contact']['company_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company name'),
      '#default_value' => $config->get('company_name') ?? NULL,
    ];

    $form['tabs']['client_info']['contact']['company_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company description'),
      '#default_value' => $config->get('company_description') ?? NULL,
    ];

    $form['tabs']['client_info']['contact']['street'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Street + housenumber'),
      '#default_value' => $config->get('street') ?? NULL,
    ];

    $form['tabs']['client_info']['contact']['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postal code'),
      '#default_value' => $config->get('postal_code') ?? NULL,
    ];

    $form['tabs']['client_info']['contact']['locality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Locality'),
      '#default_value' => $config->get('locality') ?? NULL,
    ];

    $form['tabs']['client_info']['contact']['country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country'),
      '#default_value' => $config->get('country') ?? NULL,
    ];

    $form['tabs']['client_info']['contact']['country_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country code'),
      '#default_value' => $config->get('country_code') ?? NULL,
    ];

    $form['tabs']['client_info']['contact']['phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone'),
      '#default_value' => $config->get('phone') ?? NULL,
    ];

    $form['tabs']['client_info']['contact']['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail'),
      '#default_value' => $config->get('email') ?? NULL,
    ];

    // Social Media profiles
    $form['tabs']['client_info']['social_media'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Social media'),
    ];

    $social_channels = [
      'facebook' => 'Facebook',
      'linkedin' => 'LinkedIn',
      'x' => 'X',
      'instagram' => 'Instagram',
      'pinterest' => 'Pinterest',
      'whatsapp' => 'Whatsapp',
      'tiktok' => 'TikTok',
    ];

    foreach ($social_channels as $social_key => $social_name) {
      $form['tabs']['client_info']['social_media']['social_media_' . $social_key] = [
        '#type' => 'textfield',
        '#title' => $social_name,
        '#default_value' => $config->get('social_media_' . $social_key) ?? NULL,
      ];
    }

    // Predefined pages tab.
    $form['tabs']['predefined_pages'] = [
      '#type' => 'details',
      '#title' => $this->t('Predefined pages'),
      '#group' => 'tabs',
    ];

    // Privacy/disclaimer
    $form['tabs']['predefined_pages']['privacy_disclaimer'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Privacy & disclaimer'),
    ];

    if ($config->get('page_privacy')) {
      $privacy_page = \Drupal::entityTypeManager()->getStorage('node')
        ->load($config->get('page_privacy'));
    }

    $form['tabs']['predefined_pages']['privacy_disclaimer']['page_privacy'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Privacy page '),
      '#target_type' => 'node',
      '#tags' => TRUE,
      '#size' => 30,
      '#maxlength' => 1024,
      '#default_value' => $privacy_page ?? NULL,
    ];

    if ($config->get('page_disclaimer')) {
      $disclaimer_page = \Drupal::entityTypeManager()->getStorage('node')
        ->load($config->get('page_disclaimer'));
    }

    $form['tabs']['predefined_pages']['privacy_disclaimer']['page_disclaimer'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Disclaimer page '),
      '#target_type' => 'node',
      '#tags' => TRUE,
      '#size' => 30,
      '#maxlength' => 1024,
      '#default_value' => $disclaimer_page ?? NULL,
    ];

    // Google Business Profile tab.
    $form['tabs']['google_business_profile'] = [
      '#type' => 'details',
      '#title' => $this->t('Google Business Profile'),
      '#group' => 'tabs',
    ];

    // GBP fieldset
    $form['tabs']['google_business_profile']['google_business_profile'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Google Business Profile'),
    ];

    $form['tabs']['google_business_profile']['google_business_profile']['google_place_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Place ID'),
      '#default_value' => $config->get('google_place_id') ?? NULL,
      '#description' => $this->t('The Google Maps Place ID from the location you want to see reviews for. Find the place id of you location at <a href=":link">Google Place ID Finder</a>.', [':link' => 'https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder']),
    ];

    $form['tabs']['google_business_profile']['google_business_profile']['google_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Maps Places API URL'),
      '#default_value' => $config->get('google_api_url') ?? 'https://maps.googleapis.com/maps/api/place/details/json',
      '#description' => $this->t('The Google Maps Places API URL.'),
    ];

    $form['tabs']['google_business_profile']['google_business_profile']['google_auth_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Auth key'),
      '#default_value' => $config->get('google_auth_key') ?? NULL,
      '#description' => $this->t('Your Google API key from Google Maps API. To obtain a key you need to create a project in the Google Cloud Console, <a href=":link">see documentation</a>.', [':link' => 'https://developers.google.com/maps/documentation/embed/get-api-key']),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('vb_core.site_info_config.settings');

    $data = [];
    foreach ($values as $key => $value) {
      $data[$key] = $value;

      // Override, we only need node id.
      if (str_starts_with($key, 'page_') && isset($value[0]['target_id'])) {
        $data[$key] = $value[0]['target_id'];
      }
    }

    $config->setData($data);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
