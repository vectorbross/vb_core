<?php

namespace Drupal\vb_core\Constants;

/**
 * Class VbCoreConstants.
 *
 * Class containing the constants for vb_core.
 */
class VbCoreConstants {

  /**
   * vb_core constants constants.
   */
  const VB_ADD_REVIEW_URL = 'https://g.page/r/CYcYD7sGThdiEAE/review';
  const VB_FEEDBACK_HELP_MAIL = 'help@v-b.be';

}
