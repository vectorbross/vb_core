<?php

namespace Drupal\vb_core;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;

class EntityAutocompleteMatcher extends \Drupal\Core\Entity\EntityAutocompleteMatcher {

  /**
   * Gets matched labels based on a given search string.
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {

    $matches = [];

    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler' => $selection_handler,
    ];

    $handler = $this->selectionManager->getInstance($options);

    if (isset($string)) {
      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, 10);

      // Loop through the entities and convert them into autocomplete output.
        foreach ($entity_labels as $values) {
          foreach ($values as $entity_id => $label) {

            $entity = \Drupal::entityTypeManager()->getStorage($target_type)->load($entity_id);
            $entity = \Drupal::service('entity.repository')->getTranslationFromContext($entity);

            $type = !empty($entity->type->entity) ? $entity->type->entity->label() : $entity->bundle();
            $status = '';
            if ($entity->getEntityType()->id() == 'node') {
              $status = ($entity->isPublished()) ? ", Published" : ", Unpublished";
            }

            // Also render parent-terms in autocomplete.
            $prepend_items = [];
            if ($entity->getEntityType()->id() == 'taxonomy_term') {
              $parents = \Drupal::entityTypeManager()->getStorage($target_type)->loadAllParents($entity_id);
              foreach ($parents as $key => $parent_term) {
                if ($key != $entity->id()) {
                  array_unshift($prepend_items, $parent_term->getName());
                }
              }
            }

            $key = $label . ' (' . $entity_id . ')';
            // Strip things like starting/trailing white spaces, line breaks and tags.
            $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
            // Names containing commas or quotes must be wrapped in quotes.
            $key = Tags::encode($key);
            if (!empty($prepend_items)) {
              $label = implode(' > ', $prepend_items) . ' > ' . $label;
            }
            $label = $label . ' (' . $entity_id . ') [' . $type . $status . ']';
            $matches[] = ['value' => $key, 'label' => $label];
          }
        }
    }

    return $matches;
  }

}
