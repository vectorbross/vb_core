<?php

declare(strict_types=1);

namespace Drupal\vb_core_google_review;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a google review structured data node reference entity type.
 */
interface GoogleReviewStructuredDataNodeReferenceInterface extends ContentEntityInterface {

}
