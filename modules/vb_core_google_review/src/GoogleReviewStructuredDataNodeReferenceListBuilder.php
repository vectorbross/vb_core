<?php

declare(strict_types=1);

namespace Drupal\vb_core_google_review;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\node\NodeInterface;

/**
 * Provides a list controller for the google review structured data node reference entity type.
 */
final class GoogleReviewStructuredDataNodeReferenceListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['node'] = $this->t('Node');
    $header['language'] = $this->t('Language');
    $header['product_description'] = $this->t('Product description');
    $header['brand_name'] = $this->t('Brand name');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\vb_core_google_review\GoogleReviewStructuredDataNodeReferenceInterface $entity */
    $row['id'] = $entity->id();
    $node_reference = $entity->get('field_node_reference')->getValue()[0]['target_id'];
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($node_reference);
    if ($node instanceof NodeInterface) {
      $row['node'] = $node->toLink();
    }

    $row['language'] = $entity->language()->getName();
    $row['product_description'] = $entity->get('field_product_description')->getString();
    $row['brand_name'] = $entity->get('field_brand_name')->getString();

    return $row + parent::buildRow($entity);
  }

}
