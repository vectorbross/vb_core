<?php

namespace Drupal\vb_core_google_review\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Google review results' block.
 *
 * @Block(
 *   id = "google_review_results_block",
 *   admin_label = @Translation("Google review results Block"),
 *   category = @Translation("Vector BROSS"),
 * )
 */
class GoogleReviewResultsBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'show_score' => TRUE,
      'show_stars' => TRUE,
      'google_reviews_link' => NULL,
      'score' => NULL,
      'reviews_count' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['show_score'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show score'),
      '#default_value' => $config['show_score'],
    ];

    $form['show_stars'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show stars'),
      '#default_value' => $config['show_stars'],
    ];

    $form['google_reviews_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google reviews url'),
      '#default_value' => $config['google_reviews_link'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['show_score'] = $values['show_score'];
    $this->configuration['show_stars'] = $values['show_stars'];
    $this->configuration['google_reviews_link'] = $values['google_reviews_link'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get Google reviews.
    $reviews = \Drupal::service('vb_core_google_review.helper')->getGoogleReviews();
    $config = $this->getConfiguration();

    if (isset($reviews['rating']) && isset($reviews['user_ratings_total'])) {
      $build = [
        '#theme' => 'google_review_results',
        '#show_score' => $config['show_score'],
        '#show_stars' => $config['show_stars'],
        '#google_reviews_link' => $config['google_reviews_link'],
        '#score' => $reviews['rating'],
        '#reviews_count' => $reviews['user_ratings_total'],
      ];
    }

    // Cache for max. 1 day.
    $build['#cache']['max-age'] = 86400;

    return $build;

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
