<?php

declare(strict_types=1);

namespace Drupal\vb_core_google_review\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\vb_core_google_review\GoogleReviewStructuredDataNodeReferenceInterface;

/**
 * Defines the google review structured data node reference entity class.
 *
 * @ContentEntityType(
 *   id = "vb_core_grsd_node_reference",
 *   label = @Translation("Google review structured data node reference"),
 *   label_collection = @Translation("Google review structured data node references"),
 *   label_singular = @Translation("google review structured data node reference"),
 *   label_plural = @Translation("google review structured data node references"),
 *   label_count = @PluralTranslation(
 *     singular = "@count google review structured data node references",
 *     plural = "@count google review structured data node references",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\vb_core_google_review\GoogleReviewStructuredDataNodeReferenceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\vb_core_google_review\GoogleReviewStructuredDataNodeReferenceAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\vb_core_google_review\Form\GoogleReviewStructuredDataNodeReferenceForm",
 *       "edit" = "Drupal\vb_core_google_review\Form\GoogleReviewStructuredDataNodeReferenceForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vb_core_google_review\Routing\GoogleReviewStructuredDataNodeReferenceHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "vb_core_grsd_node_reference",
 *   data_table = "vb_core_grsd_node_reference_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer vb_core_grsd_node_reference",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/vb-core-grsd-node-reference",
 *     "add-form" = "/vb-core-grsd-node-reference/add",
 *     "canonical" = "/vb-core-grsd-node-reference/{vb_core_grsd_node_reference}",
 *     "edit-form" = "/vb-core-grsd-node-reference/{vb_core_grsd_node_reference}",
 *     "delete-form" = "/vb-core-grsd-node-reference/{vb_core_grsd_node_reference}/delete",
 *     "delete-multiple-form" = "/admin/content/vb-core-grsd-node-reference/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.vb_core_grsd_node_reference.settings",
 * )
 */
final class GoogleReviewStructuredDataNodeReference extends ContentEntityBase implements GoogleReviewStructuredDataNodeReferenceInterface {

}
