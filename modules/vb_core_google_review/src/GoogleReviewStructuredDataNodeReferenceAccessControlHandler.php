<?php

declare(strict_types=1);

namespace Drupal\vb_core_google_review;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the google review structured data node reference entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class GoogleReviewStructuredDataNodeReferenceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view vb_core_grsd_node_reference', 'administer vb_core_grsd_node_reference'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit vb_core_grsd_node_reference', 'administer vb_core_grsd_node_reference'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete vb_core_grsd_node_reference', 'administer vb_core_grsd_node_reference'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create vb_core_grsd_node_reference', 'administer vb_core_grsd_node_reference'], 'OR');
  }

}
