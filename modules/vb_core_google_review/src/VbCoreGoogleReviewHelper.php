<?php

namespace Drupal\vb_core_google_review;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Helper-class for vb_core.
 */
class VbCoreGoogleReviewHelper {

  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The URL generator.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a GetGoogleData object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   */
  public function __construct(ClientInterface $client, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger, ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, UrlGeneratorInterface $url_generator, TranslationInterface $string_translation,  CacheBackendInterface $cache) {
    $this->client = $client;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->urlGenerator = $url_generator;
    $this->stringTranslation = $string_translation;
    $this->cache = $cache;
  }

  /**
   * Adds custom structured data to attachments.
   *
   * Determine which attachments need to be attached
   * and attach them.
   *
   * @param array $attachments
   *   The attachments that are modified.
   */
  public function addCustomStructuredData(array &$attachments) {
    // Get current node and check if there are vb_core_grsd_node_reference entities.
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $storage = \Drupal::entityTypeManager()->getStorage('vb_core_grsd_node_reference');
      $references = $storage->loadByProperties([
        'field_node_reference' => $node->id(),
        'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
      ]);

      if (!empty($references)) {
        $grsd_node_reference = reset($references);
        $attachments = $this->addProductReviewStructuredData($attachments, $grsd_node_reference);
      }
    }

    return $attachments;
  }

  /**
   * Adds product review structured data attachments.
   *
   * Determine which attachments need to be attached
   * and attach them.
   *
   * @param array $attachments
   *   The attachments that are modified.
   */
  private function addProductReviewStructuredData(array &$attachments, $grsd_node_reference) {
    $config = $this->configFactory->get('vb_core.site_info_config.settings');
    $google_reviews = $this->getGoogleReviews();

    $structured_data = [
      '@context' => 'https://schema.org/',
      '@type' => 'Product',
      'name' => $config->get('company_name'),
      'image' => '',
      'description' => $grsd_node_reference->get('field_product_description')->getString(),
      'brand' => [
        '@type' => 'Brand',
        'name' => $grsd_node_reference->get('field_brand_name')->getString(),
      ],
      'aggregateRating' => [
        '@type' => 'aggregateRating',
        'ratingValue' => $google_reviews['rating'] ?? '',
        'ratingCount' => $google_reviews['user_ratings_total'] ?? '',
      ],
    ];

    $attachments['#attached']['html_head'][] = [
      [
        '#type' => 'html_tag',
        '#tag' => 'script',
        '#value' => json_encode($structured_data, JSON_UNESCAPED_UNICODE),
        '#attributes' => ['type' => 'application/ld+json'],
      ],
      'page_product_review_rating',
    ];

    return $attachments;
  }

  /**
   * Import new Google reviews into review content-type, if exists.
   * @return void
   */
  public function importGoogleReviews() {
    $new_reviews = [];
    // First we check if vb_reviews exists.
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('vb_reviews')) {
      $resultArray = $this->getGoogleReviews();
      if (isset($resultArray['reviews']) && !empty($resultArray['reviews'])) {
        foreach ($resultArray['reviews'] as $review) {
          if (isset($review['time'])) {
            $review_nodes = $node_storage->loadByProperties(['type' => 'review', 'created' => $review['time']]);
            if (empty($review_nodes)) {
              // Does not exist, create new review node.
              $review_node = Node::create([
                'type' => 'review',
                'uid' => 1,
                'status' => 0,
                'title' => $review['author_name'],
                'created' => $review['time']
              ]);

              $review_node->set('field_review_rating', $review['rating']);
              $review_node->set('field_review_author_name', $review['author_name']);
              $review_node->set('field_review_author_url', $review['author_url']);
              $review_node->set('field_review_original_language', $review['original_language']);
              $review_node->set('field_review_text', ['value' => $review['text'], 'format' => 'full_html']);

              $profile_image_media = $this->createMediaImageFromUrl($review['profile_photo_url'], 'review-' . $review['time'] . '.jpg');
              $field_media_image = [
                'target_id' => $profile_image_media->id(),
              ];
              $review_node->set('field_review_author_profile_img', $field_media_image);

              $review_node->save();
              $new_reviews[] = $review_node;
            }
          }
        }
      }
      // If new reviews imported, send email.
      if (isset($new_reviews) && !empty($new_reviews)) {
        // Send email to site email-address. @todo.
        /** @var MailManagerInterface $mail_manager */
        $mail_manager = \Drupal::service('plugin.manager.mail');
        $to = \Drupal::config('system.site')->get('mail');
        $langcode = \Drupal::languageManager()->getDefaultLanguage()->getId();
        $params['message'][] = t('New imported Google-reviews are available for validation on your website:');

        foreach ($new_reviews as $new_review) {
          $edit_review = $new_review->toUrl('edit-form', ['absolute' => TRUE])->toString();
          $params['message'][] = $new_review->getTitle() . ': ' . $edit_review;
        }

        $result = $mail_manager->mail('vb_core_google_review', 'new_reviews_imported', $to, $langcode, $params, NULL, TRUE);
      }
    }
  }

  /**
   * Helper to create Media entity from url and filename.
   *
   * @param $url
   * @param $filename
   * @return \Drupal\Core\Entity\ContentEntityBase|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|Media|void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createMediaImageFromUrl($url, $filename) {
    $arrContextOptions= [
      'ssl' => [
        'verify_peer' => false,
        'verify_peer_name' => false,
      ],
    ];
    $file_data = file_get_contents($url, false, stream_context_create($arrContextOptions));

    if ($file_data) {
      $directory = 'public://';
      $file_system = \Drupal::service('file_system');
      $file_system->prepareDirectory($directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

      /** @var \Drupal\file\FileRepositoryInterface $fileRepository */
      $fileRepository = \Drupal::service('file.repository');
      $file = $fileRepository->writeData($file_data, $directory . $filename, FileSystemInterface::EXISTS_REPLACE);

      $image_media = Media::create([
        'name' => $filename,
        'bundle' => 'image',
        'uid' => 1,
        'field_media_image' => [
          'target_id' => $file->id(),
        ],
      ]);

      $image_media->save();

      return $image_media;
    }
  }

  /**
   * Get reviews from Google Maps API.
   *
   * @param array $fields
   *   (optional) The fields which the result should be limited to.
   * @param int $max_reviews
   *   (optional) The max amount of reviews to return.
   * @param string $reviews_sort
   *   (optional) The sorting of the reviews 'newest' or 'most_relevant'.
   * @param string $language
   *   (optional) The language that should be used to translate certain results.
   *
   * @return array
   *   Data from Google Maps API with information about a place_id in an array.
   */
  public function getGoogleReviews(array $fields = [], int $max_reviews = 5, string $reviews_sort = 'newest', string $language = ''): array {
    $cache = $this->cache->get('vb_core_google_reviews');

    if (isset($cache->data)) {
      return $cache->data;
    }

    $config = $this->configFactory->get('vb_core.site_info_config.settings');
    $auth_key = $config->get('google_auth_key');
    $place_id = $config->get('google_place_id');
    $api_url = $config->get('google_api_url');

    if ($auth_key == '' || $place_id == '') {
      $link = $this->urlGenerator->generateFromRoute('vb_core.site_information');
      $this->messenger->addError($this->t('You need to add your Google Business Profile credentials on the <a href=":link">Site information settings page</a>.', [':link' => $link]));
      return [];
    }

    $url_parameters = [
      'place_id' => $place_id,
      'key' => $auth_key,
      'reviews_sort' => $reviews_sort,
      //'language' => ($language == '') ? $this->languageManager->getCurrentLanguage()->getId() : $language,
      'reviews_no_translations' => TRUE,
      'translated' => FALSE,
    ];

    if (!empty($fields)) {
      $url_parameters['fields'] = implode(',', $fields);
    }

    try {
      $result = [];
      $request = $this->client->get($api_url, ['query' => $url_parameters]);
      $resultArray = json_decode($request->getBody(), TRUE);

      if ($resultArray['status'] !== 'OK') {
        if (isset($resultArray['error_message']) && !empty($resultArray['error_message'])) {
          $this->messenger->addError($this->t('Something went wrong with contacting the Google Maps API. @status, @error', [
            '@status' => $resultArray['status'],
            '@error' => $resultArray['error_message'],
          ]));
        }
        else {
          $this->messenger->addError($this->t('Something went wrong with contacting the Google Maps API: @status', [
            '@status' => $resultArray['status'],
          ]));
        }
      }

      if (isset($resultArray['result']) && !empty($resultArray['result'])) {
        if (isset($resultArray['result']['reviews'])) {
          $resultArray['result']['reviews'] = array_slice($resultArray['result']['reviews'], 0, $max_reviews);
        }

        $result = $resultArray['result'];
        $result['place_id'] = $place_id;
      }

      // Cache data for 1 day.
      $this->cache->set('vb_core_google_reviews', $result, time() + 86400, []);
      return $result;
    }
    catch (RequestException $e) {
      $this->messenger->addError($this->t('Something went wrong with contacting the Google Maps API.'));
    }

    return [];
  }

}
