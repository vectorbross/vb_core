/**
 * @file
 * Transforms links into a chosen list.
 */

(function ($) {

  'use strict';

  Drupal.facets = Drupal.facets || {};
  Drupal.behaviors.facetsChosenWidget = {
    attach: function (context, settings) {
      Drupal.facets.makeChosen(context, settings);
    }
  };

  /**
   * Turns all facet links into a chosen with options for every link.
   *
   * @param {object} context
   *   Context.
   * @param {object} settings
   *   Settings.
   */
  Drupal.facets.makeChosen = function (context, settings) {
    // Find all chosen facet links and turn them into an option.
    $('.js-facets-chosen-links').once('facets-chosen-transform').each(function () {
      var $ul = $(this);
      var $links = $ul.find('.facet-item a');
      var $chosen = $('<select />');
      // Preserve all attributes of the list.
      $ul.each(function() {
        $.each(this.attributes,function(idx, elem) {
            $chosen.attr(elem.name, elem.value);
        });
      });
      // Remove the class which we are using for .once().
      $chosen.removeClass('js-facets-chosen-links');

      $chosen.addClass('facets-chosen');
      $chosen.addClass('js-facets-widget');
      $chosen.addClass('js-facets-chosen');

      var id = $(this).data('drupal-facet-id');
      // Add aria-labelledby attribute to reference label.
      $chosen.attr('aria-labelledby', "facet_"+id+"_label");
      var default_option_label = settings.facets.chosen_widget[id]['facet-default-option-label'];

      // Add empty text option first.
      var $default_option = $('<option />')
        .attr('value', '')
        .text(default_option_label);
      $chosen.append($default_option);

      $ul.prepend('<li class="default-option"><a href=".">' + default_option_label + '</a></li>');

      var has_active = false;
      $links.each(function () {
        var $link = $(this);
        var active = $link.hasClass('is-active');
        var $option = $('<option />')
          .attr('value', $link.attr('href'))
          .data($link.data());
        if (active) {
          has_active = true;
          // Set empty text value to this link to unselect facet.
          $default_option.attr('value', $link.attr('href'));
          $ul.find('.default-option a').attr("href", $link.attr('href'));
          $option.attr('selected', 'selected');
          $link.find('.js-facet-deactivate').remove();
        }
        $option.text($link.text());
        $chosen.append($option);
      });

      // Initiate chosen
      //$chosen.chosen();

      // Go to the selected option when it's clicked.
      $chosen.on('change.facets', function () {
        var anchor = $($ul).find("[data-drupal-facet-item-id='" + $(this).find(':selected').data('drupalFacetItemId') + "']");
        var $linkElement = (anchor.length > 0) ? $(anchor) : $ul.find('.default-option a');
        var url = $linkElement.attr('href');

        $(this).trigger('facets_filter', [ url ]);
      });

      // Append empty text option.
      if (!has_active) {
        $default_option.attr('selected', 'selected');
      }

      // Replace links with chosen.
      $ul.after($chosen).hide();
      Drupal.attachBehaviors($chosen.parent()[0], Drupal.settings);
    });
  };

})(jQuery);
